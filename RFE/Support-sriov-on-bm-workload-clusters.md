# Enhancement/Feature description

Performance oriented CNF oftenly relies on usage of SR-IOv virtual functions, on workload clusters with baremetal worker nodes. We need to be able to allocate such SR-IOv virtual functions to pods.

# Technical implementation

Usage of SR-IOv devices by pods in the context of k8s clusters deployed on baremetal servers is mature, and it seems that all CNF vendors are relying on the same mechanisms.

Those pods have to request for for specific SR-IOv resources in their specification.

At the infrastrucure level, some components have to be put in place in order to create SR-IOv virtual functions on worker nodes, and to schedule pods that are requestion SR-IOv virtual functions on worker nodes that have the requested amount of virtual functions left.

For this purpose, an existing SR-IOv network operator is available, documented and maintained:

https://github.com/k8snetworkplumbingwg/sriov-network-operator/blob/master/doc/quickstart.md#configuration

It is officially supported with rke2:

https://docs.rke2.io/install/network_options/#using-multus-with-sr-iov

# How to test it

Some work is in progress at intel, led by petar Torre, to provide some scripts to test that a cluster is able to allocate some SR-IOv virtual functions to a pod, and that this pod is using those virtual functions properly. We'll rely on this work for testing our integration of the sriov-network-operator, as soon as it is ready.

